#!/usr/bin/env bash
# installs java (current default version 7) on the vagrant box
apt-get install default-jdk -y
cd /vagrant/demo
# builds the jar file of the project
./gradlew build
cd /vagrant/demo/build/libs
# runs the application using the jar file
java -jar demo-0.5.jar

