# Pre-installation Requirements:
	* download VirtualBox(current version 5.0.20) from https://www.virtualbox.org/wiki/Downloads
	* install Vagrant(current version 1.8.1) from https://www.vagrantup.com/downloads.html

# Installation Procedure:
	* clone repository to local machine
	* open command prompt/terminal
	* cd to the root folder of the downloaded repository
	* run command "vagrant up"
	* open favorite browser
	* type localhost:8089 to open the Hello World application